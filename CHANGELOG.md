# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.7.0] - 2023-04-08

### Changed

- API version

### Removed

- Bundled dependencies

### Fixed

- Settings handler incorrectly loading defaults

## [1.6.3] - 2020-08-10

### Removed

- Unused dependency

## [1.6.2] - 2020-08-10

### Removed

- Crafting spinner module (ZOS added their own)

### Fixed

- Stable timer duration

## [1.6.1] - 2020-07-20

### Fixed

- Sync timers on reload

## [1.6.0] - 2020-07-18

### Updated

- Brought codebase up to date

## [1.5.1] - 2018-08-23

### Fixed

- Spinner 'nil' bug

## [1.5.0] - 2018-08-19

### Added

- Jewelry crafting support

## [1.4.1] - 2018-08-17

### Updated

- API version

## [1.4.0] - 2017-05-23

### Added

- Spinner module

### Updated

- API version for Morrowind

## [1.3.0] - 2017-05-16

### Added

- Support for custom timer notification sounds
- Slash command to set custom unofficial notification sounds
- Per-skill color options
- Ability to lock the position of the UI
- Option to display completed or remaining percentage instead of time remaining

### Changed

- Improved handling of enabling account-wide settings for the first time

### Fixed

- Bug which caused traits to be improperly identified in certain instances
- Minor typo in "Carry Capacity"

## [1.2.3] - 2017-05-07

### Fixed

- Typo in carry capacity riding trait

## [1.2.2] - 2017-05-06

### Fixed

- Incorrectly getting trait indexes

## [1.2.1] - 2017-05-05

### Fixed

- Riding timer abbreviation bug for stamina and capacity
- Drain option completion background bug
- Multi-character completed research bug

## [1.2.0] - 2017-05-03

### Added

- Option to display timers with a reverse fill (drain) effect
- Option to abbreviate skill and trait names
- Detail to stable timers

### Improved

- Settings layout

## [1.2.0] - 2017-05-01

### Added

- Keybinding to toggle timer display

### Changed

- Improved announcement notification for stable timers

## [1.1.1] - 2017-04-28

### Fixed

- Chat notification display

## [1.1.0] - 2017-04-27

### Added

- Notification support
- /scholar timers command

### Fixed

- Minor bug fixes

## [1.0.0] - 2017-04-26

### Added

- Stable training timer

### Fixed

- String bug

## [0.0.1] - 2017-04-23

### Added

- Initial release
