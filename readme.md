# Scholar

<!-- markdownlint-disable MD013 -->
[![License: GPL-3.0+](https://img.shields.io/badge/license-GPL--3.0%2B-blue.svg)][License]
[![Open Issues](https://img.shields.io/gitlab/issues/open-raw/widgitlabs/eso/Scholar?gitlab_url=https%3A%2F%2Fgitlab.com%2F)][Open Issues]
[![Pipelines](https://gitlab.com/widgitlabs/eso/Scholar/badges/main/pipeline.svg)][Pipelines]
[![Discord](https://img.shields.io/discord/586467473503813633?color=899AF9)][Discord]
<!-- markdownlint-restore MD013 -->

Scholar is sort of the spiritual successor to [Harven's Research Timers]. While
Harven's addon is outstanding, it's always had a few quirks that annoyed me,
and a few features that I felt were missing. Thus, I decided to experiment with
a partial fork that did what I wanted it to, and Scholar was born.

Scholar displays progress bars for all of the traits you are currently
researching, along with a live timer for each. However... I've added a few
things, changed a few things, and reworked a ton of the codebase (mostly
to facilitate future functionality).

The settings panel has been completely reworked, pre-existing settings are now
easily configurable (such as font and text style selection), a few things that
weren't previously configurable now are, and those settings that require
reloading the UI now have simple, builtin methods to do so.

Right now, Scholar is pretty simple, but I want to take it a lot farther...
I want it to be a full-scale, modular crafting and research enhancement addon.
But, to do that, I need feedback! What can be improved with the existing setup?
What would you like to see added? Let me know, and I'll see what I can do!

## Support

This is a developer's portal for Scholar and should _not_ be used for support. Please visit the [support page](http://www.esoui.com/portal.php?uid=31772&a=listbugs) if you need to submit a support request.

[License]: https://gitlab.com/widgitlabs/eso/Scholar/blob/main/license.txt
[Open Issues]: https://gitlab.com/widgitlabs/eso/Scholar/-/boards
[Pipelines]: https://gitlab.com/widgitlabs/eso/Scholar/pipelines
[Discord]: https://discord.gg/jrydFBP
[Harven's Research Timers]: http://www.esoui.com/downloads/fileinfo.php?id=521&so=DESC#info
